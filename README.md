**TODO:**
1. Auto enable Lets Encrypt on port 443
2. Disable 000-default
3. Enable nextcloud-443.conf
4. Update php.ini with opcache settings
    opcache.enable=1
    opcache.enable_cli=1
    opcache.interned_strings_buffer=8
    opcache.max_accelerated_files=10000
    opcache.memory_consumption=128
    opcache.save_comments=1
    opcache.revalidate_freq=1

# Nextcloud + Lets Encrypt
A quick search on hub.docker.com show several images for Nextcloud already, why another one? Because I want built in support for HTTPS using Let's Encrypt.

## Caveats
1. This image is built with apache instead of nginx because, for small installs there isn't a major performance difference, and I don't know nginx
2. This image is built with Ubuntu 16.04 base because I don't know alpine
3. Though it is possible to run everything manually, I would recomend the use of a docker-compose file (included below) because of the use of a lot of enviroment variables

## Installation
docker build . --pull --rm -t michaelholley/nextcloud:12.0.0

## Usage
docker network create -d bridge --subnet 192.168.12.0/24 nextcloud_nw

docker run --name nextcloud \
-h files.holley.us \
-e EMAIL=michael@holley.us \
--network nextcloud_nw \
-v /data01/docker/nextcloud/config:/var/www/nextcloud/config \
-v /data01/docker/nextcloud/apps:/var/www/nextcloud/apps \
-v /data01/docker/nextcloud/data:/var/www/nextcloud/data \
-v /data01/docker/nextcloud/apache:/etc/apache2/sites-available \
-v /data01/docker/nextcloud/ssl:/etc/letsencrypt \
-p 443:443 \
-d michaelholley/nextcloud:12.0.0

docker run --name mariadb \
-e MYSQL_ROOT_PASSWORD=root-database-password \
-e MYSQL_DATABASE=nextclouddb \
-e MYSQL_USER=nextclouduser \
-e MYSQL_PASSWORD=nextcloud-user-password \
--network nextcloud_nw \
-v /data01/docker/mariadb:/var/lib/mysql \
-d mariadb:latest
