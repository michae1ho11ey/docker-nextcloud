FROM ubuntu:latest
ENV DEBIAN_FRONTEND noninteractive

## Install Prerequisits for Nextcloud, Let's Encrypt, and useful tools; then clean up
RUN apt-get update && apt-get install -y --no-install-recommends \
    apache2 \
    libapache2-mod-php \
    php-dom \
    php-gd \
    php-xml \
    php-zip \
    php-mcrypt \
    php-mysql \
    php-sqlite3 \
    php-bz2 \
    php-intl \
    php-imap \
    php-curl \
    php-gmp \
    php-apcu \
    php-imagick \
    php-mbstring \
    ffmpeg \
    cron \
    bzip2 \
    curl \
    python-letsencrypt-apache \
    python \
    python-dev \
    virtualenv \
    python-virtualenv \
    gcc \
    dialog \
    libaugeas0 \
    augeas-lenses \
    libssl-dev libffi-dev \
    ca-certificates && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    apt-get autoremove -y

## Configure Apache
RUN a2enmod rewrite && \
    a2enmod headers && \
    a2enmod env && \
    a2enmod dir && \
    a2enmod mime && \
    a2enmod ssl

## Download Nextcloud
ENV VERSION 12.0.3
RUN curl -o /tmp/nextcloud-$VERSION.tar.bz2 https://download.nextcloud.com/server/releases/nextcloud-$VERSION.tar.bz2

COPY bootstrap.sh /bootstrap.sh
ENTRYPOINT ["/bootstrap.sh"]
EXPOSE 80 443
VOLUME ["/var/www/nextcloud", "/var/www/nextcloud-data", "/etc/letsencrypt", "/etc/apache2/sites-available"]

CMD ["apache2ctl", "-DFOREGROUND"]
