#!/bin/bash -e
## Download/verify Nextcloud then install
if [ ! -f /var/www/nextcloud/version.php ]; then
   TARBALL=`ls /tmp |grep nextcloud`

   echo "Found and extracting the ${TARBALL} tarball"
   tar -xjf /tmp/${TARBALL} -C /var/www
   rm /tmp/${TARBALL}*

   echo "Site extracted and changing permissions"
   mkdir -p /var/www/nextcloud-data
   chmod 0755 /var/www/nextcloud /var/www/nextcloud-data /var/www/nextcloud/apps /var/www/nextcloud/config
   chown -R www-data:www-data /var/www/nextcloud*

   if [ -d /var/www/nextcloud ]; then
      echo "Nextcloud has been installed"
   else
      echo "Something broke and Nextcloud wasn't installed"
   fi
fi

## Check for and install Nextcloud cron
if [ ! -f /etc/cron.d/nextcloud ]; then
   echo "*/15 * * * * www-data /usr/bin/php -f /var/www/nextcloud/cron.php" > /etc/cron.d/nextcloud
   echo "Nextcloud's cron has been installed"
fi

## Install the Nextcloud apache conf file
if [ ! -f /etc/apache2/sites-available/nextcloud.conf ]; then
   {
     echo '<VirtualHost *:80>'; \
     echo "  ServerName $HOSTNAME"; \
     echo "  ServerAdmin $EMAIL"; \
     echo '  DocumentRoot /var/www/nextcloud'; \
     echo ''; \
     echo '  <Directory /var/www/nextcloud/>'; \
     echo '      Options +FollowSymlinks'; \
     echo '      AllowOverride All'; \
     echo '      Satisfy Any'; \
     echo '      <IfModule mod_dav.c>'; \
     echo '          Dav off'; \
     echo '      </IfModule>'; \
     echo '      SetEnv HOME /var/www/nextcloud'; \
     echo '      SetEnv HTTP_HOME /var/www/nextcloud'; \
     echo '  </Directory>'; \
     echo '  <Directory /var/www/nextcloud-data/>'; \
     echo '      AllowOverride All'; \
     echo '      Order deny,allow'; \
     echo '      deny from all'; \
     echo '  </Directory>'; \
     echo ''; \
     echo '  <FilesMatch "\.(cgi|shtml|phtml|php)$">'; \
     echo '      SSLOptions +StdEnvVars'; \
     echo '  </FilesMatch>'; \
     echo '  <Directory /usr/lib/cgi-bin>'; \
     echo '      SSLOptions +StdEnvVars'; \
     echo '  </Directory>'; \
     echo ''; \
     echo "  ErrorLog ${APACHE_LOG_DIR}/${HOSTNAME}_error.log"; \
     echo "  CustomLog ${APACHE_LOG_DIR}/${HOSTNAME}_access.log combined"; \
     echo '</VirtualHost>'; \
   } > /etc/apache2/sites-available/nextcloud.conf

   if [ -f /etc/apache2/sites-available/nextcloud.conf ]; then
      echo "The Apache conf for Nextcloud has been installed"
      a2dissite 000-default && a2ensite nextcloud
      echo "ServerName $HOSTNAME" >> /etc/apache2/apache2.conf
   else
      echo "The Apache conf file wasn't created and nothing will work"
   fi
fi

# Generate Let's Encrypt TLS Cert and auto-renewal cron
if [ $ENABLE_SSL == "true" ]; then
	if [ ! -f /etc/letsencrypt/live/${HOSTNAME}/privkey.pem ]; then
   		letsencrypt --non-interactive --agree-tos --email $EMAIL --apache --redirect --domains $HOSTNAME
	else
   		echo "Found existing cert"
	fi

	if [ ! -f /etc/cron.d/certbot ]; then
   		echo "1 */12 * * * root letsencrypt renew --quiet" > /etc/cron.d/certbot
   		echo "Letsencrypt cron has been installed"
	fi
fi

# Make optimiziations to php.ini file
APACHE_INI="/etc/php/7.0/apache2/php.ini"
if [ ! `grep "opcache.enable=1" $APACHE_INI` ]; then #opcache.enable
    sed -i 's/;opcache.enable=.*/opcache.enable=1/'
fi

if [ ! `grep "opcache.enable_cli=1" $APACHE_INI` ]; then #opcache.enable_cli
    sed -i 's/;opcache.enable_cli=.*/opcache.enable_cli=1/'
fi

if [ ! `grep "opcache.memory_consumption=128" $APACHE_INI` ]; then  #opcache.memory_consumption
    sed -i 's/;opcache.memory_consumption=.*/opcache.memory_consumption=128/'
fi

if [ ! `grep "opcache.interned_strings_buffer=8" $APACHE_INI` ]; then #opcache.interned_strings_buffer
    sed -i 's/;opcache.interned_strings_buffer=.*/opcache.interned_strings_buffer=8/'
fi

if [ ! `grep "opcache.max_accelerated_files=10000" $APACHE_INI` ]; then #opcache.max_accelerated_files
    sed -i 's/;opcache.max_accelerated_files=.*/opcache.max_accelerated_files=10000/'
fi

if [ ! `grep "opcache.revalidate_freq=1" $APACHE_INI` ]; then #opcache.revalidate_freq
    sed -i 's/;opcache.revalidate_freq=.*/opcache.revalidate_freq=1/'
fi

if [ ! `grep "opcache.save_comments=1" $APACHE_INI` ]; then #opcache.save_comments
    sed -i 's/;opcache.save_comments=.*/opcache.save_comments=1/'
fi

# Update config.php with additional options
curl localhost > /dev/null
CONFIG_PATH="/var/www/nextcloud/config/config.php"
if [ -e $CONFIG_PATH ] && [[ ! `grep cron_log $CONFIG_PATH` ]]; then
    LINES=`wc -l < $CONFIG_PATH`

    if [ -e $CONFIG_PATH ]; then
       sed -i "${LINES}i\\  'cron_log' => true,\\" $CONFIG_PATH
       sed -i "${LINES}i\\  'memcache.local' => '\OC\Memcache\APCu',\\" $CONFIG_PATH
    fi
fi

# Remove old PID if container didn't terminate cleanly
if [ -f /run/apache2/apache2.pid ]; then
   rm /run/apache2/apache2.pid
fi

## Start up all required services
service cron start
echo "Cron service started up"
echo "Starting up Apache"
exec "$@"
